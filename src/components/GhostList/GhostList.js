import { Box } from '@material-ui/core';
import React from 'react';
import GhostCard from '../GhostCard/GhostCard';

const GhostList = ({ filteredGhosts, filteredEvidences }) => (
  <Box>
    {filteredGhosts.map((ghost) => (
      <GhostCard key={ghost.type} ghost={ghost} filteredEvidences={filteredEvidences}></GhostCard>
    ))}
  </Box>
);

export default GhostList;
