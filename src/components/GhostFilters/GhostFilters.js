import { Box, Button, styled } from '@material-ui/core';
import React from 'react';
import { EVIDENCE_TYPES } from '../../utils/constants';
import filterGhosts from '../../utils/filters';

const GhostFilterWrapper = styled(Box)({
  margin: '20px 10px 10px 0',
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'space-between',
  columnCount: 2,
});

const StyledButton = styled(Button)({
  padding: '8px 15px',
  margin: '0 0 10px 10px',
  flex: ({ extraWidth }) => (extraWidth ? '1 1 34%' : '1 1 26%'),
  justifyContent: 'left',
});

StyledButton.defaultProps = {
  edge: 'start',
};

const GhostFilters = ({ evidences, filteredGhosts, updateEvidence }) => {
  const isFilterable = (evidenceTypeKey) =>
    filterGhosts(filteredGhosts, { ...evidences, [evidenceTypeKey]: true }).length > 0;

  const GhostFilter = ({ evidenceType }) => {
    const { key } = evidenceType;
    const filterable = isFilterable(key);

    return (
      <StyledButton
        extraWidth={evidenceType.extraWidth}
        variant={evidences[key] ? 'contained' : 'outlined'}
        color={evidences[key] ? 'primary' : 'default'}
        startIcon={<evidenceType.icon />}
        onClick={() => updateEvidence({ [key]: !evidences[key] })}
        disabled={!filterable}
      >
        {evidenceType.label}
      </StyledButton>
    );
  };

  return (
    <GhostFilterWrapper>
      <GhostFilter evidenceType={EVIDENCE_TYPES.FINGERPRINTS} />
      <GhostFilter evidenceType={EVIDENCE_TYPES.FREEZING_TEMPERATURE} />
      <GhostFilter evidenceType={EVIDENCE_TYPES.EMF5} />
      <GhostFilter evidenceType={EVIDENCE_TYPES.GHOST_WRITING} />
      <GhostFilter evidenceType={EVIDENCE_TYPES.GHOST_ORBS} />
      <GhostFilter evidenceType={EVIDENCE_TYPES.SPIRIT_BOX} />
    </GhostFilterWrapper>
  );
};

export default GhostFilters;
