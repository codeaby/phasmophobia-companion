import React from 'react';
import { Box, styled } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import { EVIDENCE_TYPES } from '../../utils/constants';

const EvidencesWrapper = styled(Box)({
  float: 'right',
});

const StyledIcon = styled(Icon)(({ theme, filtered }) => ({
  marginLeft: 4,
  padding: 5,
  borderRadius: 4,
  backgroundColor: filtered ? theme.palette.primary.main : 'action',
}));

StyledIcon.defaultProps = {
  fontSize: 'large',
};

const GhostEvidences = ({ evidences, filteredEvidences }) => {
  const Evidence = ({ evidenceType }) => {
    const { key, icon } = evidenceType;

    return evidences[key] && <StyledIcon component={icon} filtered={filteredEvidences[key]} />;
  };

  return (
    <EvidencesWrapper>
      <Evidence evidenceType={EVIDENCE_TYPES.FINGERPRINTS} />
      <Evidence evidenceType={EVIDENCE_TYPES.FREEZING_TEMPERATURE} />
      <Evidence evidenceType={EVIDENCE_TYPES.GHOST_WRITING} />
      <Evidence evidenceType={EVIDENCE_TYPES.EMF5} />
      <Evidence evidenceType={EVIDENCE_TYPES.GHOST_ORBS} />
      <Evidence evidenceType={EVIDENCE_TYPES.SPIRIT_BOX} />
    </EvidencesWrapper>
  );
};

export default GhostEvidences;
