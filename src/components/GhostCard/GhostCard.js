import { Card, CardContent, Typography } from '@material-ui/core';
import { styled } from '@material-ui/core/styles';
import React from 'react';
import GhostEvidences from '../GhostEvidences/GhostEvidences';

const CardWrapper = styled(Card)({
  margin: '10px',
});

const StyledCardContent = styled(CardContent)({
  paddingBottom: '16px !important',
});

const GhostCard = ({ ghost, filteredEvidences }) => {
  return (
    <CardWrapper>
      <StyledCardContent>
        <Typography variant="h5" component="h2">
          {ghost.type}
          <GhostEvidences evidences={ghost.evidences} filteredEvidences={filteredEvidences} />
        </Typography>
        <Typography style={{ marginBottom: 8, marginTop: 16 }}>
          <b>Puntos fuertes:</b> {ghost.strengths}
        </Typography>
        <Typography>
          <b>Puntos debiles:</b> {ghost.weaknesses}
        </Typography>
      </StyledCardContent>
    </CardWrapper>
  );
};

export default GhostCard;
