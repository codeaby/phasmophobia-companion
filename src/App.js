import Delete from '@material-ui/icons/Delete';
import {
  AppBar,
  IconButton,
  ThemeProvider,
  Toolbar,
  Typography,
  createMuiTheme,
  CssBaseline,
} from '@material-ui/core';
import GhostList from './components/GhostList/GhostList';
import GhostFilters from './components/GhostFilters/GhostFilters';
import { useState } from 'react';
import { EVIDENCE_TYPES, GHOSTS } from './utils/constants';
import filterGhosts from './utils/filters';

const initialState = Object.keys(EVIDENCE_TYPES).reduce(
  (state, key) => ({ ...state, [EVIDENCE_TYPES[key].key]: false }),
  {}
);

function App() {
  const theme = createMuiTheme({
    palette: {
      type: 'dark',
    },
  });

  const [evidences, setEvidences] = useState(initialState);
  const [ghosts, setGhosts] = useState(GHOSTS);

  const updateEvidence = (newEvidences) => {
    const filteredEvidences = {
      ...evidences,
      ...newEvidences,
    };
    setEvidences(filteredEvidences);
    setGhosts(filterGhosts(GHOSTS, filteredEvidences));
  };

  const resetEvidence = () => {
    setEvidences(initialState);
    setGhosts(GHOSTS);
  };

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" style={{ flexGrow: 1 }}>
            Phasmophobia Companion
          </Typography>
          <IconButton edge="end" color="inherit" aria-label="menu" onClick={resetEvidence}>
            <Delete />
          </IconButton>
        </Toolbar>
      </AppBar>
      <GhostFilters evidences={evidences} filteredGhosts={ghosts} updateEvidence={updateEvidence} />
      <GhostList filteredGhosts={ghosts} filteredEvidences={evidences} />
    </ThemeProvider>
  );
}

export default App;
