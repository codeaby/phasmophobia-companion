import { AcUnit, BlurOn, Create, NetworkCheck, PanTool, Radio } from '@material-ui/icons';
import phasmophobia from '../phasmophobia.json';

export const GHOSTS = phasmophobia.ghosts;

export const EVIDENCE_TYPES = {
  EMF5: { key: 'emf5', label: 'EMF5', icon: NetworkCheck },
  FINGERPRINTS: { key: 'fingerprints', label: 'Huellas', extraWidth: true, icon: PanTool },
  FREEZING_TEMPERATURE: { key: 'freezingTemperature', label: 'Temp.', icon: AcUnit },
  GHOST_ORBS: { key: 'ghostOrbs', label: 'Orbes', icon: BlurOn },
  GHOST_WRITING: { key: 'ghostWriting', label: 'Escritura', extraWidth: true, icon: Create },
  SPIRIT_BOX: { key: 'spiritBox', label: 'Spirit', icon: Radio },
};
