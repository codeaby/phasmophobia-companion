import { EVIDENCE_TYPES } from './constants';

const checkEvidence = (evidenceName, ghost, filters) =>
  !filters[evidenceName] || (filters[evidenceName] && ghost.evidences[evidenceName]);

const filterGhosts = (ghosts, filters) => {
  return ghosts.filter((ghost) =>
    Object.keys(EVIDENCE_TYPES).reduce(
      (shouldShow, key) => shouldShow && checkEvidence(EVIDENCE_TYPES[key].key, ghost, filters),
      true
    )
  );
};

export default filterGhosts;
